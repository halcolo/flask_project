from flask import Flask
from flask import request

# Set the server and run in the terminal
# Python 3.6
# Port: 8005

app = Flask(__name__) #New object

@app.route('/')#Wrap and route
def index():
    return 'Hellow world'#Return String

@app.route('/params')
def params():
    params = request.args.get('params1', 'It not contains params1')
    return 'Hello, your param is: {}'.format(params)

if __name__ == '__main__':
    app.run(debug= True, port= 8085 ) #Run the server, set
