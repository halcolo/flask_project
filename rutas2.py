from flask import Flask
from flask import request

# Set the server and run in the terminal
# Python 3.6
# Port: 8005

app = Flask(__name__) #New object

@app.route('/')#Wrap and route
def index():
    return 'Hellow world'#Return String

@app.route('/params/')
@app.route('/params/<name>/')
@app.route('/params/<name>/<int:num>')
def params(name = 'Default', num= 0):
    return 'Hello, your param is: {} {}'.format(name, num)

if __name__ == '__main__':
    app.run(debug= True, port= 8085 ) #Run the server, set
