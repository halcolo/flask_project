from flask import Flask
from flask import request
from flask import render_template

# Set the server and run in the terminal
# Python 3.6
# Port: 8005

app = Flask(__name__, template_folder = 'templates') #Se requiere un objeto

'''@app.route('/')#Wrap o un decorador
def index():
    return render_template('index.html') #Regresa un String
'''
@app.route('/user/')
@app.route('/user/<name>')
def user(name = 'default'):
    age = 19
    my_list = [1,2,3,4]
    return render_template('user.html', name=name, age=age, list=my_list)

if __name__ == '__main__':
    app.run(debug= True, port= 8085 ) #Run the server, set
